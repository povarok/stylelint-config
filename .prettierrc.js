module.exports = {
  bracketSpacing: true,
  // jsxBracketSameLine: true, //causes bug!
  bracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  printWidth: 120,
};
